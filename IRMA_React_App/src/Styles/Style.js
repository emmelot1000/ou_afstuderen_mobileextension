import { StyleSheet } from 'react-native';

module.exports = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#99D3D9",
        padding: 5
    },
    textStyle: {
        color: "black",
        fontSize: 18
    },
    buttonStyle: {
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
        backgroundColor: "#244c66",
        color: "white",
        textAlign: "center",
        fontSize: 25,
        marginTop: 100,
        marginBottom: 50
    },
    buttonStyleSmall:{
        fontSize: 14,
        padding: 5,
        marginTop: 0,
        marginBottom: 0
    },
    disabledButtonStyle: {
        backgroundColor: "#614245",
        color: "#919191"
    },
    topHeader: {
        fontSize: 50,
        textAlign: "center",
        color: "white",
        fontWeight: "bold"
    },
    subHeader: {
        fontSize: 25,
        textAlign: "center",
        color: "black"
    },
    wrapLeft: {
        flex: 0.7
    }, 
    wrapRight:{
        flex: 0.3
    },
    row: {
        flex: 1,
        flexDirection: "row",
        marginVertical: 10
    }
})    