import React from 'react';
import {Provider} from 'react-redux';
import { store } from "./Redux/Store"
import IrmaScanner from "./Redux/IrmaScanner" ;

/**
 * The actual app
 */
export default function App() {
  return (
    <Provider store = {store}>
      <IrmaScanner/>
    </Provider>
  )
}