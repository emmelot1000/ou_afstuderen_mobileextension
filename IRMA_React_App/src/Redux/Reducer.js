import { State } from "react-native-ble-plx"
import BluetoothDevice from "../Data/BluetoothDevice"

//List of all types of actions which can be received
export type Action =
  | UpdateScanningStateAction
  | BleStateUpdatedAction
  | IrmaScannerFoundAction

export type BleStateUpdatedAction = {|
  type: "BLE_STATE_UPDATED",
    state: $Keys<typeof State>
|}

export type UpdateScanningStateAction = {|
  type: "SCANNING_STATE_UPDATED",
    state: $Keys<typeof ScanningState>
|}

export type IrmaScannerFoundAction = {|
  type: "IRMA_SCANNER_FOUND",
    device: BluetoothDevice
      |}

export type ReduxState = {
  activeIrmaScanner: ?BluetoothDevice,
  scanningState: $Keys<typeof ScanningState>,
  bleState: $Keys<typeof State>
}

export const ScanningState = {
  SCANNING: "START_SCANNING",
  IDLE: "IDLE"
}

export const initialState: ReduxState = {
  bleState: State.Unknown,
  activeIrmaScanner: null,
  scanningState: ScanningState.IDLE,
  devices: []
}

export function updateScanningState(
  state: $Keys<typeof ScanningState>
): UpdateScanningStateAction {
  return {
    type: "SCANNING_STATE_UPDATED",
    state
  }
}

export function bleStateUpdated(
  state: $Keys<typeof State>
): BleStateUpdatedAction {
  return {
    type: "BLE_STATE_UPDATED",
    state
  }
}

export function IrmaScannerFound(device: BluetoothDevice): IrmaScannerFoundAction{
  return {
    type: "IRMA_SCANNER_FOUND",
    device
  }
}

/**
 * Reducer function needed for saga redux 
 * @param {ReduxState} state The current state of the device 
 * @param {Action} action The action which updated a state
 */
export function reducer(
  state: ReduxState = initialState,
  action: Action
): ReduxState {
  switch (action.type) {
    case "SCANNING_STATE_UPDATED":
      return {
        ...state,
        scanningState: action.state
      }
    case "BLE_STATE_UPDATED":
      return {
        ...state,
        bleState: action.state
      }
    case "IRMA_SCANNER_FOUND":
      var index = state.devices.findIndex((el) => el.key == action.device.key);
      var list = state.devices;
      if (index >= 0) {
        list.splice(index, 1);
      }
      list = list.concat(action.device);
      return {
        ...state,
        devices: list
      }
    default:
      return state
  }
}