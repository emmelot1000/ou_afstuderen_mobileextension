import React, { Component } from "react"
import { connect as reduxConnect } from "react-redux"
import {SafeAreaView, StatusBar } from "react-native"
import {type ReduxState, ScanningState, updateScanningState } from "./Reducer"
import styles from "../Styles/Style"
import BluetoothDevice from "../Data/BluetoothDevice"
import IrmaView from "../Components/IrmaView";

type Props = {
  IrmaScanner: ?BluetoothDevice,
  scanningState: $Keys<typeof ScanningState>,
  devices: []
}

/**
 * Shows the UI and sends actions form the UI to the Saga
 */
class IrmaScanner extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.initialize();
  }

  /**
   * Initializes the scanning state
   */
  initialize = () => {
    this.scanStateChangeHandler(ScanningState.IDLE);
    this.scanSubmitHandler();
  }

  //Initial scan state is idle
  state = {
    scanState: ScanningState.IDLE,
    title: 'Start Scanning'
  }

  /**
   * Add scan state to props
   */
  scanSubmitHandler = () => {
    this.props.add(this.state.scanState);
  }

  /**
   * Update value of scan state
   */
  scanStateChangeHandler = (value) => {
    this.state.scanState = value;
  }

  /**
   * Update title of button
   */
  buttonTitleStateChangedHandler = (value) => {
    this.setState({
      title: value
    })
  }

  /**
   * Update the scan state after button was pressed
   */
  scanStateUpdate = (newState) => {
    this.scanStateChangeHandler(newState);
    this.scanSubmitHandler();
    var buttonText = newState === ScanningState.SCANNING ? "Scanning..." : "Start Scanning";
    this.buttonTitleStateChangedHandler(buttonText);
  }

  /**
   * Sends a scanStateUpdate based on the current state
   */
  onPressScanButton = () => {
    if (this.state.scanState == ScanningState.IDLE) {
      this.scanStateUpdate(ScanningState.SCANNING);
    } else {
      this.scanStateUpdate(ScanningState.IDLE);
    }
  }

  /**
   * Renders the UI
   */
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#5a070f" />
        <IrmaView
          title={this.state.title}
          devices={this.props.devices}
          onPressMethod={this.onPressScanButton} />
      </SafeAreaView>
    )
  }
}

/**
 * Updates the scanning state
 * @param {Object} dispatch The dispatched object
 */
const mapDispatchToProps = dispatch => {
  return {
    add: (scanning) => {
      dispatch(updateScanningState(scanning))
    }
  }
}

/**
 * Sets the device list to the new list
 * @param {Object} state Contains the device list
 */
function mapStateToProps(state) {
  return {
    devices: state.devices
  }
}

export default reduxConnect(mapStateToProps, mapDispatchToProps)(IrmaScanner)