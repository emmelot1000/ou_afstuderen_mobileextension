import { PermissionsAndroid, Platform } from "react-native"
import { buffers, eventChannel, END } from "redux-saga"
import { fork, cancel, take, call, put, cancelled, actionChannel } from "redux-saga/effects"
import { bleStateUpdated, type BleStateUpdatedAction, type UpdateScanningStateAction, IrmaScannerFound, ScanningState } from "./Reducer"
import { BleManager, BleError, Device, State } from "react-native-ble-plx"
import BluetoothDevice from "../Data/BluetoothDevice"

//Global variables containing all bluetooth devices
var bluetoothDevices = [];

//Redux-saga is a redux middleware library for handling side effects in redux app.
//This is done by using Generators, which allow us to write asynchronous code that looks synchronous.
export function* bleSaga(): Generator<*, *, *> {
  // Create BleManager, which is the entry point to all BLE related functionalities
  const manager = new BleManager()

  // Create generators for handling scanning and handling BLE state changes
  yield fork(handleScanning, manager)
  yield fork(handleBleState, manager)
}

// This generator tracks our BLE state. Based on that we can enable scanning, get rid of devices etc.
// eventChannel allows us to wrap callback based API which can be then conveniently used in sagas.
function* handleBleState(manager: BleManager): Generator<*, *, *> {
  const stateChannel = yield eventChannel((emit) => {
    const subscription = manager.onStateChange((state) => {
      emit(state)
    }, true)
    return () => {
      subscription.remove()
    }
  }, buffers.expanding(1))

  try {
    for (; ;) {
      const newState = yield take(stateChannel)
      yield put(bleStateUpdated(newState))
    }
  } finally {
    if (yield cancelled()) {
      stateChannel.close()
    }
  }
}

// This generator decides if we want to start or stop scanning depending on specific events:
// * BLE state is in PoweredOn state
// * Android's permissions for scanning are granted
// * We already scanned device which we wanted
function* handleScanning(manager: BleManager): Generator<*, *, *> {
  var scanTask = null
  var bleState: $Keys<typeof State> = State.Unknown
  var scanningState: $Keys<typeof ScanningState> = ScanningState.IDLE

  //Listen to the following actions
  const channel = yield actionChannel([
    "SCANNING_STATE_UPDATED",
    "BLE_STATE_UPDATED"
  ])

  //Constantly check state of device to determine if it is ready for scanning
  for (; ;) {
    console.log('Checking state')
    const action:
      | UpdateScanningStateAction
      | BleStateUpdatedAction = yield take(channel)

    switch (action.type) {
      case "BLE_STATE_UPDATED":
        bleState = action.state
        break
      case "SCANNING_STATE_UPDATED":
        scanningState = action.state
        break
    }

    //Determine if device can start scanning
    const enableScanning = (scanningState == ScanningState.SCANNING) && (bleState == State.PoweredOn)

    //If device is ready for scanning, stop previous scan task if existed and start new one
    if (enableScanning) {
      if (scanTask != null) {
        yield cancel(scanTask)
      }
      scanTask = yield fork(scan, manager)
      //If device is not ready for scanning, stop previous scan if existed
    } else {
      if (scanTask != null) {
        yield cancel(scanTask)
        scanTask = null
      }
    }
  }
}

// As long as this generator is working we have enabled scanning functionality.
// When we detect IrmaScanner device we make it as an active device.
function* scan(manager: BleManager): Generator<*, *, *> {
  console.log('Start scanning');

  //Check if Android permissions have been given
  if (Platform.OS === "android" && Platform.Version >= 23) {
    const enabled = yield call(
      PermissionsAndroid.check,
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
    )
    //If permision have not been given, ask for permissions
    if (!enabled) {
      const granted = yield call(
        PermissionsAndroid.request,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      )
      //Stop scanning if now permissions have been granted
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        return
      }
    } else {
      console.log('Permissions were granted');
    }
  }

  console.log('Starting channel');
  //scanningChannel will get value with scanned device when device is found
  const scanningChannel = yield eventChannel((emit) => {
    manager.startDeviceScan(
      null,     //Array of UUIDs which are registered in scanned device
      //if null is passed: all available devices will be scanned
      { allowDuplicates: true },
      (error, scannedDevice) => {   //Called when a device has been found. 
        if (error) {
          emit([error, scannedDevice])
          return
        }
        if (scannedDevice != null) {
          //Process the found device and check if it is an IRMA antenna
          var bluetoothDevice = findDevice(scannedDevice);
          //If it is an IRMA antenna and the URL is not yet sent to the UI
          if (bluetoothDevice && !bluetoothDevice.device.getSendComplete()) {            
            var urlPresent = bluetoothDevice.device.checkUrl(scannedDevice.manufacturerData);
            //If the IRMA antenna is not yet visible on the UI, it is sent
            if(!bluetoothDevice.device.getAlreadySent()){
              bluetoothDevice.device.setAlreadySent(true);
              emit([error,scannedDevice]);
            }      
            //If an URL is present, the button on the UI is being enabled      
            if (urlPresent) {
              bluetoothDevice.device.setSendComplete(true);
              emit([error, scannedDevice]);
            }
          }
        }
      }
    )
    return () => {
      console.log('Stopping scan');
      manager.stopDeviceScan()
    }
  }, buffers.expanding(1))

  try {
    for (; ;) {
      const [error, scannedDevice]: [?BleError,?Device] = yield take(
        scanningChannel
      )
      if (scannedDevice != null) {
        var bluetoothDevice = findDevice(scannedDevice);
        yield put(IrmaScannerFound(bluetoothDevice))
      }
    }
  } catch (error) {
    console.log('caught error', error);
  } finally {
    if (yield cancelled()) {
      scanningChannel.close()
    }
  }
}

/**
 * Searches for the device in the list with device. 
 * If the device is not present, a new device is created
 * @param {Device} device The device that needs to be found
 * @returns {Object} Contains the key and the BluetoothDevice
 */
function findDevice(device) {
  if (device.manufacturerData && device.manufacturerData.substring(0, 1) == "/") {
    var uuid = device.id;
    var dev = bluetoothDevices.find((el) => el.key == uuid);
    if (!dev) {
      dev = { key: uuid, device: new BluetoothDevice(device) };
      bluetoothDevices.push(dev);
    }
    return dev;
  } else {
    return null;
  }
}

