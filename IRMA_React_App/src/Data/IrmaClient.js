import { startIntent } from "../Redux/IrmaIntent"

/**
 * Starts an unique IRMA session
 * @param {String} url The url for the session
 */
export async function startUniqueSession(url) {
    var httpUrl = "http://" + url;
    var index = httpUrl.lastIndexOf('/') + 1;
    var sessionToken = httpUrl.substring(index);
    var sessionUrl = httpUrl.substring(0, index);
    sessionUrl += ('broadcast/' + sessionToken + '/unique');
    //Retrieve the session data for the unique session and start the IRMA app with unique data
    getSessionData(sessionUrl).then(function (result) {
        getSession(result).then(function (sessionData) {
            startIntent(sessionData);
        });
    });
}

/**
 * Retrieves the session data for the unique IRMA session
 * @returns {Promise} A promise with the session data
 */
getSessionData = function (url) {
    var promise = new Promise(function (resolve, reject) {
        fetch(url)
            .then((response) => resolve(response));
    })
    return promise;
};

/**
 * Retrieves the session for the unique IRMA session
 * @returns {Promise} A promise with the session
 */
getSession = function (response) {
    var promise = new Promise(function (resolve, reject) {
        response.json()
            .then((result) => resolve(result));
    })
    return promise;
}