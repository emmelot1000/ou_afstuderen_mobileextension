var SendIntentAndroid = require('react-native-send-intent');

/**
 * Starts an IRMA intent
 * @param {Object} sessionData Contains the session data for the intent
 */
export function startIntent(sessionData) {
    const url = 'qr/json/' + encodeURIComponent(JSON.stringify(sessionData));
    const intent = "intent://" + url;
    SendIntentAndroid.openApp('org.irmacard.cardemu', intent).then((wasOpened) => {console.log("IRMA app was opened.")});
}