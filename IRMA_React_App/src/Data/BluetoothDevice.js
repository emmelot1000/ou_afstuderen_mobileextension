import { Device } from "react-native-ble-plx"
import { decode as atob} from 'base-64'

/**
 * Class containing a bluetooth device and all required functions
 */
export default class BluetoothDevice {

    /**
     * Creates a Bluetooth Device object
     * @param {Device} device The actual device object
     */
    constructor(device) {
        this.device = device;
        this.receivedAdvertisements = [];
        this.alreadySent = false;
        this.url = "";
        this.totalMessages = 0;
        this.sendComplete = false;
    }

    /**
     * Retrieves the ID of this device
     * @returns {String} The id of this device
     */
    getId() {
        return this.device.id;
    }

    /**
     * Sets the url for this device 
     * @param {String} url The url for this device
     */
    setUrl(url) {
        this.url = url;
    }

    /**
     * Retrieves the url for this device
     * @returns {String} The url of this device
     */
    getUrl() {
        return this.url;
    }

    /**
     * Retrieves the boolean that indicates of this device is already sent to the UI
     * @returns {Boolean} The already sent indicator
     */
    getAlreadySent() {
        return this.alreadySent;
    }

    /**
     * Sets the indicator already sent
     * @param {Boolean} alreadySent The new value for the indicator
     */
    setAlreadySent(alreadySent) {
        this.alreadySent = alreadySent;
    }

    /**
     * Retrieves te boolean that indicates of this device has been sent to the UI as complete
     * @returns {Boolean} The sent complete indicator
     */
    getSendComplete(){
        return this.sendComplete;
    }

    /**
     * Sets the indicator sent complete
     * @param {Boolean} sendComplete The new value for the indicator
     */
    setSendComplete(sendComplete){
        this.sendComplete = sendComplete;
    }

    /**
     * Determines whether the IRMA button should be enabled or not
     * @returns {Bool} Indicates enabling the button or not
     */
    getEnableButton(){
        return (this.getSendComplete() && this.getAlreadySent());
    }

    /**
     * Checks if the url already exists
     * @param {String} data The message to be decoded
     * @returns {Boolean} Indicates if the url is ready or not
     */
    checkUrl(data) {
        if (!this.getSendComplete()) {
            //Decode the base64 message, ignoring first two characters
            var decodedMessage = atob(data).substring(2);
            if (!this.receivedAdvertisements.find(x => x == decodedMessage)) {
                //Add message to received and sort based on sequence number
                this.receivedAdvertisements.push(decodedMessage);
                this.receivedAdvertisements.sort();
                //Check if last message is available
                if (decodedMessage.indexOf("URLEND") > 0) {
                    this.totalMessages = parseInt(decodedMessage.substring(0, decodedMessage.indexOf(".")));
                }
                if (this.totalMessages > 0 && this.receivedAdvertisements.length === this.totalMessages) {
                    //All the message are found. Create the url and add it to the class
                    var url = this.createURL();
                    this.setUrl(url);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Creates the URL by joining all received advertisements
     * @returns {String} The url of the advertisements
     */
    createURL() {
        //Filter out the element with end string
        let withoutEndString = this.receivedAdvertisements.filter(x => x.indexOf("URLEND") < 0);
        //Filter out the sequence numbers
        let withoutSeqNumbers = withoutEndString.map(x => x.substring(x.indexOf(".") + 1));
        //Join the pieces and remove by join inserted commas
        let url = withoutSeqNumbers.join().replace(/,/g, '');
        this.receivedAdvertisements = [];
        return url;
    }
}