import React, { Component } from "react";
import { Text, TouchableOpacity } from "react-native";
import styles from "../Styles/Style";

/**
 * A button that can be used in the UI
 */
class Button extends Component {    
    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={this.props.style} >
                <Text
                    style={[
                        styles.buttonStyle,
                        this.props.disabled ? styles.disabledButtonStyle : null,
                        this.props.small ? styles.buttonStyleSmall : null
                    ]}>
                    {this.props.title}
                </Text>
            </TouchableOpacity>
        )
    }
}

export default Button;