import React, {Component} from "react";
import { View } from "react-native";
import Button from "./Button";
import Header from "./Header";
import DeviceList from "./DeviceList";


/**
 * The main view for the IRMA application
 */
class IrmaView extends Component {
  render(){
    return (
      <View style={{padding:10}}>
      <Header/>
      <View style = {{flexDirection: "row", paddingTop: 5}}>
        <Button 
          style={{flex: 1}}
          small={false}
          onPress={this.props.onPressMethod}
          title = {this.props.title}
          />
      </View>
      <DeviceList
        list = {this.props.devices}
        />
      </View>
    )
  }
}

export default IrmaView;