import React, {Component} from "react";
import {FlatList} from "react-native";
import DeviceItem from "./DeviceItem";

/**
 * A list containing all the devives that can be used in the UI
 */
class DeviceList extends Component {
    render() {
        return (
            <FlatList
                data = {this.props.list}
                renderItem = { ({item}) => <DeviceItem
                                                name={item.key}
                                                url={item.device.getUrl()}
                                                disable={!item.device.getEnableButton()}/>}
            />
        )
    }
}

export default DeviceList