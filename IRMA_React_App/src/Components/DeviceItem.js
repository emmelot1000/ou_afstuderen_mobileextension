import React, { Component } from "react";
import { Text, View } from "react-native";
import styles from "../Styles/Style";
import Button from "./Button";
import startIntent from "../Data/IrmaIntent";

/**
 * An item that can be used within a list
 */
class DeviceItem extends Component {
    render() {
        return (
            <View style={styles.row}>
                <View style={styles.wrapLeft}>
                    <Text style={styles.textStyle}>{this.props.name}</Text>
                </View>
                <View style={styles.wrapRight}>
                    <Button
                        title={"Open IRMA"}
                        small={true}
                        onPress={() => {
                            startIntent(this.props.url);
                        }}
                        disabled={this.props.disable} />
                </View>
            </View>
        )
    }
}

export default DeviceItem;