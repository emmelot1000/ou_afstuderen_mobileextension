import React, { Component } from "react";
import { Text, View } from "react-native";
import styles from "../Styles/Style";

/**
 * A header that can be used in the UI
 */
class Header extends Component {
    render(){
        return(
            <View>
                <Text style = {styles.topHeader}>IRMA</Text>
                <Text style = {styles.subHeader}>Broadcast</Text>
            </View>
        )
    }
}

export default Header;